/**
 * 
 */
package com.angejardin.microserviceusers.controllers;

/**
 * @author Audrey Ledoux
 *
 */
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.angejardin.microserviceusers.models.ERole;
import com.angejardin.microserviceusers.models.Role;
import com.angejardin.microserviceusers.models.User;
import com.angejardin.microserviceusers.payload.request.LoginRequest;
import com.angejardin.microserviceusers.payload.request.SignupRequest;
import com.angejardin.microserviceusers.payload.response.JwtResponse;
import com.angejardin.microserviceusers.payload.response.MessageResponse;
import com.angejardin.microserviceusers.repository.RoleRepository;
import com.angejardin.microserviceusers.repository.UserRepository;
import com.angejardin.microserviceusers.security.jwt.JwtUtils;
import com.angejardin.microserviceusers.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	// Pour sortir de l'information du processus (erreur, avertissement, historique,
	// ...)
	Logger log = LoggerFactory.getLogger(this.getClass());// Création du log pour l'affichage de message dans la console
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		
		if (!userRepository.existsByUsername(loginRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Cet utilisateur n'existe pas !"));
		}
		
		log.info("------> match ? : " + encoder.matches(loginRequest.getPassword(), userRepository.getpassword(loginRequest.getUsername())));
		
		if (!encoder.matches(loginRequest.getPassword(), userRepository.getpassword(loginRequest.getUsername()))) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Le mot de passe est erroné"));
		}

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getUsername()));
	}
	

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Cet utilisateur existe déjà !"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Cet email est déjà utilisé !"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(), 
							 encoder.encode(signUpRequest.getPassword()),
							 signUpRequest.getFirstname(),
							 signUpRequest.getLastname(),
							 signUpRequest.getEmail(),
							 signUpRequest.getBirthday(),
							 signUpRequest.getCodepostal(),
							 signUpRequest.getVille(),
							 signUpRequest.getCodeinsee());
		
		Set<Role> roles = new HashSet<>();
		Role userRole = roleRepository.findByName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		roles.add(userRole);
		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("Utilisateur enregistré !"));
	}
}