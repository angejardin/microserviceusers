/**
 * @author Audrey Ledoux
 *
 */
package com.angejardin.microserviceusers.controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.angejardin.microserviceusers.models.Jardin;
import com.angejardin.microserviceusers.models.User;
import com.angejardin.microserviceusers.repository.JardinRepository;
import com.angejardin.microserviceusers.repository.UserRepository;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/data")
public class DataController {

	// Pour sortir de l'information du processus (erreur, avertissement, historique,
	// ...)
	Logger log = LoggerFactory.getLogger(this.getClass());// Création du log pour l'affichage de message dans la console

	@Autowired
	private UserRepository userrepository;

	@Autowired
	private JardinRepository jardinrepository;
	
	@Autowired
	PasswordEncoder encoder;

	@GetMapping("/profile/{username}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public Optional<User> userProfile(@PathVariable String username) {
		return userrepository.findByUsername(username);
	}

	@DeleteMapping("/profile/{userid}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<User> userDeleteProfile(@PathVariable String userid) {
		try {
			jardinrepository.deleteByUserid(Long.parseLong(userid));
			userrepository.deleteById(Long.parseLong(userid));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/profilePassword/{userid}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<User> userUpdateProfile(@PathVariable("userid") long userid, @RequestBody String password) {
		Optional<User> userData = userrepository.findById(userid);

		if (userData.isPresent()) {
			User _user = userData.get();
			_user.setPassword(encoder.encode(password));
			return new ResponseEntity<>(userrepository.save(_user), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}	
	
	@PutMapping("/profile/{userid}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<User> userUpdateProfile(@PathVariable("userid") long userid, @RequestBody User user) {
		Optional<User> userData = userrepository.findById(userid);

		if (userData.isPresent()) {
			User _user = userData.get();
			_user.setUsername(user.getUsername());
			_user.setFirstname(user.getFirstname());
			_user.setLastname(user.getLastname());
			_user.setEmail(user.getEmail());
			_user.setCodepostal(user.getCodepostal());
			_user.setBirthday(user.getBirthday());
			_user.setVille(user.getVille());
			_user.setCodeinsee(user.getCodeinsee());
			return new ResponseEntity<>(userrepository.save(_user), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}	

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Admin Board.";
	}

	@GetMapping("/jardin/{userid}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public Optional<Jardin> userJardin(@PathVariable Long userid) {
		return jardinrepository.findByUserid(userid);
	}

	@PostMapping(path = "/jardin")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<Void> creerJardin(@Valid @RequestBody Jardin jardin) {
		Jardin jardinAjoute = jardinrepository.save(jardin);
		if (jardinAjoute == null)
			return ResponseEntity.noContent().build();
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(jardinAjoute.getId())
				.toUri();
		return ResponseEntity.created(uri).build();
	}

	@PutMapping("/modifjardin/{userid}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<Jardin> updateJardin(@PathVariable("userid") long userid, @RequestBody Jardin jardin) {
		Optional<Jardin> jardinData = jardinrepository.findByUserid(userid);

		if (jardinData.isPresent()) {
			Jardin _jardin = jardinData.get();
			_jardin.setLegumesvoulus(jardin.getLegumesvoulus());
			_jardin.setLegumessemessa(jardin.getLegumessemessa());
			_jardin.setLegumessemessp(jardin.getLegumessemessp());
			_jardin.setLegumesplantes(jardin.getLegumesplantes());
			return new ResponseEntity<>(jardinrepository.save(_jardin), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@ApiOperation(value = "Donne la liste des codes insee distinct des utilisateurs inscrits") 
	@GetMapping(path="/listeInsee")  
    public @ResponseBody List<String> getListeInsee() {		
        return userrepository.getInsee();
    }

}
