/**
 * 
 */
package com.angejardin.microserviceusers.payload.request;

/**
 * @author Audrey Ledoux
 *
 */
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
 
public class SignupRequest {
    @NotBlank
    @Size(min = 6, max = 20)
    private String username;
    
    @NotBlank
    @Size(min = 2, max = 30)
    private String firstname;
    
    @NotBlank
    @Size(min = 2, max = 30)
    private String lastname;
 
    @NotBlank
    @Size(min = 6, max = 50)
    @Email
    private String email;
    
    @NotBlank
    @Size(max = 10)
    private String birthday;   
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
    
    @NotBlank
	@Size(max = 5)
	private String codepostal;

	@NotBlank
	@Size(max = 38)
	private String ville;
	
	@NotBlank
	@Size(max = 5)
	private String codeinsee;

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the codepostal
	 */
	public String getCodepostal() {
		return codepostal;
	}

	/**
	 * @param codepostal the codepostal to set
	 */
	public void setCodepostal(String codepostal) {
		this.codepostal = codepostal;
	}

	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @param ville the ville to set
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * @return the codeinsee
	 */
	public String getCodeinsee() {
		return codeinsee;
	}

	/**
	 * @param codeinsee the codeinsee to set
	 */
	public void setCodeinsee(String codeinsee) {
		this.codeinsee = codeinsee;
	}
  
    
}