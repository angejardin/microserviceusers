/**
 * 
 */
package com.angejardin.microserviceusers.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Audrey Ledoux
 *
 */
@Entity
@Table(name = "jardin")
public class Jardin {
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	private Long userid;
	
	@NotBlank
	private String[] legumesvoulus;
	
	@NotBlank
	private String[] legumessemessa;
	
	@NotBlank
	private String[] legumessemessp;
	
	@NotBlank
	private String[] legumesplantes;
	
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the userid
	 */
	public Long getUserid() {
		return userid;
	}

	/**
	 * @param userid the userid to set
	 */
	public void setUserid(Long userid) {
		this.userid = userid;
	}

	/**
	 * @return the legumesvoulus
	 */
	public String[] getLegumesvoulus() {
		return legumesvoulus;
	}

	/**
	 * @param legumesvoulus the legumesvoulus to set
	 */
	public void setLegumesvoulus(String[] legumesvoulus) {
		this.legumesvoulus = legumesvoulus;
	}

	
	/**
	 * @return the legumessemessa
	 */
	public String[] getLegumessemessa() {
		return legumessemessa;
	}

	/**
	 * @param legumessemessa the legumessemessa to set
	 */
	public void setLegumessemessa(String[] legumessemessa) {
		this.legumessemessa = legumessemessa;
	}

	/**
	 * @return the legumessemessp
	 */
	public String[] getLegumessemessp() {
		return legumessemessp;
	}

	/**
	 * @param legumessemessp the legumessemessp to set
	 */
	public void setLegumessemessp(String[] legumessemessp) {
		this.legumessemessp = legumessemessp;
	}

	/**
	 * @return the legumesplantes
	 */
	public String[] getLegumesplantes() {
		return legumesplantes;
	}

	/**
	 * @param legumesplantes the legumesplantes to set
	 */
	public void setLegumesplantes(String[] legumesplantes) {
		this.legumesplantes = legumesplantes;
	}

	/**
	 * @param userid
	 * @param legumesvoulus
	 * @param legumessemessa
	 * @param legumessemessp
	 * @param legumesplantes
	 */
	public Jardin(@NotBlank Long userid, @NotBlank String[] legumesvoulus, @NotBlank String[] legumessemessa,
			@NotBlank String[] legumessemessp, @NotBlank String[] legumesplantes) {
		super();
		this.userid = userid;
		this.legumesvoulus = legumesvoulus;
		this.legumessemessa = legumessemessa;
		this.legumessemessp = legumessemessp;
		this.legumesplantes = legumesplantes;
	}
	
	

	/**
	 * 
	 */
	public Jardin() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
}

