/**
 * 
 */
package com.angejardin.microserviceusers.models;

/**
 * @author Audrey Ledoux
 *
 */
public enum ERole {
	ROLE_USER,
    ROLE_ADMIN
}
