package com.angejardin.microserviceusers.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.angejardin.microserviceusers.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByUsername(String username);
	
	@Query("select u.password from User u where u.username= ?1")
    String getpassword(String name);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);

	void deleteById(Long id);
	
	@Query("select distinct u.codeinsee from User u")
    List<String> getInsee();
}