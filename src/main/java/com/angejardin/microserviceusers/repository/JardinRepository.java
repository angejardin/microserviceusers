package com.angejardin.microserviceusers.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.angejardin.microserviceusers.models.Jardin;

/**
 * @author Audrey Ledoux
 *
 */
@Repository
public interface JardinRepository extends JpaRepository<Jardin, Long> {
	Optional<Jardin> findByUserid(Long userid);

	@Transactional //pour résoudre une erreur lors de la suppression du compte de l'utilisateur
	void deleteByUserid(Long userid);	
	
}
